var mqtt = require('mqtt');
var mysql = require('mysql');
var dateFormat = require('dateformat');
var request = require('request');

const MQTT_SERVER = "35.247.128.116";
const MQTT_PORT = "1883";
const MQTT_USER = "appkin";
const MQTT_PASSWORD = "@#Appkin3122";


const TIME_LINE_NOTI_0 = ["08:00:00" , "17:00:00"];
const TIME_LINE_NOTI_1 = ["08:00:00" , "13:00:00" , "17:00:00"];


var client = mqtt.connect({
    host: MQTT_SERVER,
    port: MQTT_PORT,
    username: MQTT_USER,
    password: MQTT_PASSWORD
});

var con = mysql.createConnection({
    host: "35.240.153.210",
    user: "win",
    password: "@#WinDB2020",
    database: "win"
});



client.on('connect', function () {

    console.log("MQTT Connect");
    client.subscribe('#', function (err) {
        if (err) {
            console.log(err);
        }
    });
});



var date_diff_indays = function (date1, date2) {
  dt1 = new Date(date1);
  dt2 = new Date(date2);
  return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate() , dt2.getHours() , dt2.getMinutes(), dt2.getSeconds()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate() , dt1.getHours() , dt1.getMinutes(), dt1.getSeconds())) / (1000));
}





client.on('message', function (topic, message) {

    var topicMachine = topic;
    var json  = JSON.parse(message.toString());

    var date = dateFormat(new Date() , "yyyy/mm/dd HH:MM:ss")

    // console.log(topicMachine , date)
    
    var query = "select token_line , DATE_FORMAT(last_dt_line , '%Y/%m/%d %T') as 'dt' , noti_type from machine where uid = '"+topicMachine+"';"

    var aqiStr = "";

    if (json['aqi'] >= 0 && json['aqi'] <= 25) {
        aqiStr = "คุณภาพอากาศดีมาก"
      } else if (json['aqi'] >= 26 &&json['aqi'] <= 50) {
        aqiStr = "คุณภาพอากาศดี"
      } else if (json['aqi'] >= 51 && json['aqi'] <= 100) {
        aqiStr = "คุณภาพอากาศปานกลาง"
      } else if (json['aqi'] >= 101 && json['aqi'] <= 200) {
        aqiStr = "คุณภาพอากาศ เริ่มมีผลกระทบต่อสุขภาพ"
      } else {
        aqiStr = "คุณภาพอากาศ มีผลกระทบต่อสุขภาพ"
      }


    var message = "ค่า AQI วันนี้ : " +  + json['aqi'] + "\n( " + aqiStr + " )" + "\nค่าฝุ่น PM2.5 : " + json["pm25"]

    
    // console.log(message);

    con.query(query, function (err, result, fields) {
        if (err) {

            console.log(err)
            return;
        }
        if(Object.keys(result).length >= 1){

         


          if(result[0]["noti_type"] == 0){
          
            for(var i = 0 ; i < TIME_LINE_NOTI_0.length;i++){


              var dtRaw = dateFormat(new Date() , "yyyy/mm/dd")


              var strDTLINE = (dtRaw+" "+TIME_LINE_NOTI_0[i])
             
              var strDTServer  = result[0]["dt"]


              console.log(date);
              console.log(strDTLINE);
              console.log(strDTServer);

              console.log("-------------")


              if(date_diff_indays(strDTLINE , date) >= 1){


                if(date_diff_indays(strDTServer , strDTLINE) >= 1) {



                  console.log("NOTI");


                  request({
                    method: 'POST',
                    uri: 'https://notify-api.line.me/api/notify',
                    headers: {
                      'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    auth: {
                      'bearer': result[0]["token_line"]
                    },
                    form: {
                      message: message
                    }
                  });
    
                  query = "update machine set last_dt_line = '"+date+"' where uid = '"+topicMachine+"'; "
    
                  con.query(query, function (err, result, fields) {
                    if (err) {
            
                        console.log(err)
                        return;
                    }
    
                })


                }
              }
             
            }


          }else{
           
            for(var i = 0 ; i < TIME_LINE_NOTI_1.length;i++){

              var dtRaw = dateFormat(new Date() , "yyyy/mm/dd")


              var strDTLINE = (dtRaw+" "+TIME_LINE_NOTI_1[i])
              
              var strDTServer  = result[0]["dt"]

              console.log(strDTLINE);


              if(date_diff_indays(strDTLINE , date) >= 1){


                if(date_diff_indays(strDTServer , strDTLINE) >= 1) {



                  console.log("NOTI");


                  request({
                    method: 'POST',
                    uri: 'https://notify-api.line.me/api/notify',
                    headers: {
                      'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    auth: {
                      'bearer': result[0]["token_line"]
                    },
                    form: {
                      message: message
                    }
                  });
    
                  query = "update machine set last_dt_line = '"+date+"' where uid = '"+topicMachine+"'; "
    
                  con.query(query, function (err, result, fields) {
                    if (err) {
            
                        console.log(err)
                        return;
                    }
    
                })


                }
              }


  
  
            }


          }
          

        }

    })
});
