import 'dart:async';
import 'dart:convert';
import 'package:aqi/config.dart';
import 'package:aqi/theme/colors/light_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:progress_dialog/progress_dialog.dart';

class UIDPage extends StatefulWidget {
  UIDPage({Key key}) : super(key: key);

  @override
  UIDPageState createState() => UIDPageState();
}

enum ConfirmAction { CANCEL, ACCEPT }

class UIDPageState extends State<UIDPage> {
  ProgressDialog pr;


  TextEditingController uuid = TextEditingController();
  TextEditingController note = TextEditingController();
  void getUUID() async {
   await pr.show();
    print("UUID");
    Response response =
    await get("${Config.urlHTTP}/uuid", headers: <String, String>{
      'Accept': 'application/json; charset=UTF-8',
      'Content-Type': 'application/json; charset=UTF-8',
    });

    final jsonResponse = json.decode(response.body.toString());

    if(jsonResponse['status'] == 200){
      pr.hide();
    }
    setState(() {

      uuid.text = jsonResponse['uid'];
    });
  }


  @override
  void initState() {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Download,
      textDirection: TextDirection.ltr,
      isDismissible: false,
    );

    pr.style(
        message: "Please wait...",
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));
  }

  Future<void> _showMyDialog(String text) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text(
            'Setting',
            style: TextStyle(
              fontFamily: 'Prompt',
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("${text}",
                    style: TextStyle(
                      fontSize: 15,
                      fontFamily: 'Prompt',
                    )),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('ตกลง',
                  style: TextStyle(
                    fontFamily: 'Prompt',
                  )),
              onPressed: () {
                Navigator.of(context).pop();
//                Navigator.of(context)
//                    .pushReplacement(MaterialPageRoute(builder: (_) => LoginPage()));
              },
            ),
          ],
        );
      },
    );
  }

  Widget getButton() {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 8),
      child: Container(
        height: 48,
        margin: new EdgeInsets.symmetric(horizontal: 20.0),
        decoration: BoxDecoration(
          color: LightColors.kBlue.withOpacity(0.9),
          borderRadius: const BorderRadius.all(Radius.circular(24.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.grey.withOpacity(0.4),
              blurRadius: 8,
              offset: const Offset(4, 4),
            ),
          ],
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            highlightColor: Colors.transparent,
            onTap: () {
              if(uuid.text != null && uuid.text != ""){

                callAPI();
              }

            },
            child: Center(
              child: Text(
                'บันทึก',
                style: TextStyle(
                    fontFamily: 'Prompt',
                    fontWeight: FontWeight.w500,
                    fontSize: 18,
                    color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
  }

  callAPI() async {
    await pr.show();
    final jsonsp = jsonEncode({"uuid": uuid.text.toString(), "note": note.text.toString()});

    print(json.decode.toString());
    Response response = await post("${Config.urlHTTP}/uuid",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonsp);

    final jsonResponse = json.decode(response.body.toString());

    print(jsonResponse.toString());
    print(jsonResponse.toString());

    if (jsonResponse['status'] == 200) {
      pr.hide();
      _showMyDialog('เพิ่มสำเร็จ');
    }else if(jsonResponse['status'] == 204){
      pr.hide();
      _showMyDialog('UID ซ้ำ กดเพื่อสร้างอีกครั้ง');
    }else{
      pr.hide();
      _showMyDialog('เพิ่มไม่สำเร็จ');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: LightColors.kGreen,
        title: Text(
          "สร้าง UID",
          style: TextStyle(
            fontFamily: 'Prompt',
          ),
        ),
      ),
      backgroundColor: LightColors.kLightYellow,
      body: SafeArea(
        top: true,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 10),
            child: Column(
              children: <Widget>[
                Container(
                    child: SizedBox(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () => {
                          getUUID()
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                              left: 10.0, top: 4.0, bottom: 4.0, right: 10.0),
                          padding: EdgeInsets.only(
                              left: 16.0, top: 4.0, bottom: 4.0),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                const BorderRadius.all(Radius.circular(10.0)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.6),
                                  offset: const Offset(1.1, 1.1),
                                  blurRadius: 9.0),
                            ],
                          ),
                          child: TextField(
                            onTap: ()=>{
                              getUUID()
                            },
                            controller: uuid,
                            style: TextStyle(
                              fontFamily: 'Prompt',
                            ),
                            readOnly: true,
                            maxLines: 2,
                            keyboardType: TextInputType.multiline,
                            decoration: InputDecoration(
                                labelText: "UID กดเพื่อสร้าง",
                                labelStyle: TextStyle(
                                  fontFamily: 'Prompt',
                                ),
                                border: InputBorder.none),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 10.0, top: 4.0, bottom: 4.0, right: 10.0),
                        padding:
                            EdgeInsets.only(left: 16.0, top: 4.0, bottom: 4.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10.0)),
                          boxShadow: <BoxShadow>[
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.6),
                                offset: const Offset(1.1, 1.1),
                                blurRadius: 9.0),
                          ],
                        ),
                        child: TextField(
                          controller: note,
                          style: TextStyle(
                            fontFamily: 'Prompt',
                          ),
                          decoration: InputDecoration(
                              labelText: "หมายเหตุ",
                              labelStyle: TextStyle(
                                fontFamily: 'Prompt',
                              ),
                              border: InputBorder.none),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 10.0, top: 4.0, bottom: 4.0, right: 10.0),
                        padding:
                            EdgeInsets.only(left: 16.0, top: 4.0, bottom: 4.0),
                        child: getButton(),
                      )
                    ],
                  ),
                )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
