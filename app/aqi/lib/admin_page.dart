import 'dart:convert';
import 'package:aqi/config.dart';
import 'package:aqi/model/machine.dart';
import 'package:aqi/setting.dart';
import 'package:aqi/theme/colors/light_colors.dart';
import 'package:aqi/uid_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:http/http.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:url_launcher/url_launcher.dart';

class AdminPage extends StatefulWidget {
  const AdminPage({Key key}) : super(key: key);

  @override
  State<AdminPage> createState() => AdminPageState();
}

class AdminPageState extends State<AdminPage> {
  Machine machine;

  bool status = false;

  @override
  void initState() {
    callMachime();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => SetiingPage()));
        },
        child: Icon(Icons.add),
        backgroundColor: LightColors.kRed,
      ),
      appBar: AppBar(
          backgroundColor: LightColors.kGreen,
          title: Text("สถานที่" , style: TextStyle(fontFamily: 'Prompt')),
          actions: <Widget>[
            // action button
            IconButton(
              icon: Icon(Icons.developer_board),
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (_) => AdminUIDPage()));
              },
            ),
          ]),
      backgroundColor: LightColors.kLightGreen,
      body: (status == true)
          ? RefreshIndicator(
              color: LightColors.kRed,
              onRefresh: () async {
                callMachime();
                return;
              },
              child: Container(
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: machine.message.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                        elevation: 10.0,
                        margin: new EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 6.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                const BorderRadius.all(Radius.circular(19.0)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.0),
                                  offset: const Offset(1.1, 1.1),
                                  blurRadius: 9.0),
                            ],
                          ),
                          child: ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 10.0),
                              title: Text(
                                "${machine.message.data[index].name}",
                                style: TextStyle(
                                    fontFamily: 'Prompt',
                                    fontSize: 20,
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold),
                              ),
                              // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

                              subtitle: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Icon(Icons.device_hub,
                                          color: Colors.green),
                                      Text("สถานะอุปกรณ์ : ปกติ",
                                          style: TextStyle(
                                              fontFamily: 'Prompt',
                                              fontSize: 15,
                                              color: Colors.black87))
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      GestureDetector(
                                        onTap: () => {
                                          launch(
                                              'https://www.google.com/maps/search/?api=1&query=${machine.message.data[index].lat},${machine.message.data[index].lng}')
                                        },
                                        child: Row(
                                          children: <Widget>[
                                            Icon(Icons.map, color: Colors.blueAccent),
                                            Text("นำทาง",
                                                style: TextStyle(
                                                    fontFamily: 'Prompt',
                                                    fontSize: 15,
                                                    color: Colors.black87))
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                              trailing: IconButton(
                                onPressed: () => {
                                  deleteMachime(
                                      machine.message.data[index].name)
                                },
                                icon: Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                  size: 30,
                                ),
                              )),
                        ));
                  },
                ),
              ),
            )
          : Center(
              child: Loading(
                  indicator: BallPulseIndicator(),
                  size: 100.0,
                  color: LightColors.kRed),
            ),
    );
  }

  void callMachime() async {
    Response response =
        await get("${Config.urlHTTP}/machine", headers: <String, String>{
      'Accept': 'application/json; charset=UTF-8',
      'Content-Type': 'application/json; charset=UTF-8',
    });

    final jsonResponse = json.decode(response.body.toString());

    print("JSON ${jsonResponse.toString()}");
    setState(() {
      machine = Machine.fromJson(jsonResponse);
      print(machine.status);
      if (machine.status == 200) {
        print("wefwewef");
        status = true;
      }
    });
  }

  void deleteMachime(String name) async {
    final jsonsp = jsonEncode({"name": name});

    print(json.decode.toString());
    Response response = await post("${Config.urlHTTP}/area/delete",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonsp);

    final jsonResponse = json.decode(response.body.toString());

    if (jsonResponse['status'] == 200) {
      _showMyDialog("ลบสำเร็จ");
      callMachime();
    } else {}
  }

  Future<void> _showMyDialog(String text) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Setting'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("${text}"),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('ตกลง'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
