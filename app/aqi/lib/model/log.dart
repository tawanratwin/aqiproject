class Log {
  int status;
  List<Message> message;

  Log({this.status, this.message});

  Log.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  double pm10;
  double pm25;
  double so2;
  double o3;
  double co;
  double no2;
  String dtf;

  Message(
      {this.pm10, this.pm25, this.so2, this.o3, this.co, this.no2, this.dtf});

  Message.fromJson(Map<String, dynamic> json) {
    pm10 = json['pm10'];
    pm25 = json['pm25'];
    so2 = json['so2'];
    o3 = json['o3'];
    co = json['co'];
    no2 = json['no2'];
    dtf = json['dtf'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pm10'] = this.pm10;
    data['pm25'] = this.pm25;
    data['so2'] = this.so2;
    data['o3'] = this.o3;
    data['co'] = this.co;
    data['no2'] = this.no2;
    data['dtf'] = this.dtf;
    return data;
  }
}