class LastLog {
  int status;
  List<Message> message;

  LastLog({this.status, this.message});

  LastLog.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  int idMachine;
  int pm25;
  int pm10;
  double so2;
  double o3;
  double co;
  int no2;
  double aqi;
  String dt;

  Message(
      {this.idMachine,
        this.pm25,
        this.pm10,
        this.so2,
        this.o3,
        this.co,
        this.no2,
        this.aqi,
        this.dt});

  Message.fromJson(Map<String, dynamic> json) {
    idMachine = json['id_machine'];
    pm25 = json['pm25'];
    pm10 = json['pm10'];
    so2 = json['so2'];
    o3 = json['o3'];
    co = json['co'];
    no2 = json['no2'];
    aqi = json['aqi'];
    dt = json['dt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id_machine'] = this.idMachine;
    data['pm25'] = this.pm25;
    data['pm10'] = this.pm10;
    data['so2'] = this.so2;
    data['o3'] = this.o3;
    data['co'] = this.co;
    data['no2'] = this.no2;
    data['aqi'] = this.aqi;
    data['dt'] = this.dt;
    return data;
  }
}