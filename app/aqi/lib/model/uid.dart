class UID {
  int status;
  List<Message> message;

  UID({this.status, this.message});

  UID.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['message'] != null) {
      message = new List<Message>();
      json['message'].forEach((v) {
        message.add(new Message.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Message {
  int id;
  String text;
  String note;
  int status;

  Message({this.id, this.text, this.note, this.status});

  Message.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    text = json['text'];
    note = json['note'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['text'] = this.text;
    data['note'] = this.note;
    data['status'] = this.status;
    return data;
  }
}