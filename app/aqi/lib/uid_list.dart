import 'dart:convert';

import 'package:aqi/app_properties.dart';
import 'package:aqi/config.dart';
import 'package:aqi/detail.dart';
import 'package:aqi/model/machine.dart';
import 'package:aqi/model/uid.dart';
import 'package:aqi/setting.dart';
import 'package:aqi/theme/colors/light_colors.dart';
import 'package:aqi/uid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:http/http.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:url_launcher/url_launcher.dart';

class AdminUIDPage extends StatefulWidget {
  const AdminUIDPage({Key key}) : super(key: key);

  @override
  State<AdminUIDPage> createState() => AdminUIDPageState();
}

class AdminUIDPageState extends State<AdminUIDPage> {
  UID uid;
  ProgressDialog pr;
  bool status = false;

  @override
  void initState() {
    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Download,
      textDirection: TextDirection.ltr,
      isDismissible: false,
    );

    pr.style(
        message: "Please wait...",
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));

    callUID();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => UIDPage()));
        },
        child: Icon(Icons.add),
        backgroundColor: LightColors.kRed,
      ),
      appBar: AppBar(
        backgroundColor: LightColors.kGreen,
        title: Text("UID", style: TextStyle(fontFamily: 'Prompt')),
      ),
      backgroundColor: LightColors.kLightGreen,
      body: (status == true)
          ? RefreshIndicator(
              color: LightColors.kRed,
              onRefresh: () async {
                callUID();
                return;
              },
              child: Container(
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: uid.message.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                        elevation: 10.0,
                        margin: new EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 6.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                const BorderRadius.all(Radius.circular(19.0)),
                            boxShadow: <BoxShadow>[
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.0),
                                  offset: const Offset(1.1, 1.1),
                                  blurRadius: 9.0),
                            ],
                          ),
                          child: ListTile(
                              contentPadding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 10.0),
                              title: Text(
                                "${uid.message[index].text}",
                                style: TextStyle(
                                    fontFamily: 'Prompt',
                                    fontSize: 16,
                                    color: Colors.black87,
                                    fontWeight: FontWeight.bold),
                              ),
                              // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

                              subtitle: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Icon(Icons.device_hub,
                                          color: Colors.green),

                                      (uid.status == 1) ?
                                      Text("สถานะ UID : จับคู่แล้ว",
                                          style: TextStyle(
                                              fontFamily: 'Prompt',
                                              fontSize: 15,
                                              color: Colors.black87)) :

                                      Text("สถานะ UID : ยังไม่จับคู่",
                                        style: TextStyle(
                                            fontFamily: 'Prompt',
                                            fontSize: 15,
                                            color: Colors.black87))
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Icon(Icons.note,
                                          color: Colors.blueAccent),
                                      Text("note : ${uid.message[index].note}",
                                          style: TextStyle(
                                              fontFamily: 'Prompt',
                                              fontSize: 15,
                                              color: Colors.black87))
                                    ],
                                  ),
                                ],
                              ),
                              trailing: IconButton(
                                onPressed: () => {
                                  _showMyDialogDelete(uid.message[index].text)
//                            deleteUID(
//                                uid.message[index].text)
                                },
                                icon: Icon(
                                  Icons.delete,
                                  color: Colors.red,
                                  size: 30,
                                ),
                              )),
                        ));
                  },
                ),
              ),
            )
          : Center(
              child: Loading(
                  indicator: BallPulseIndicator(),
                  size: 100.0,
                  color: LightColors.kRed),
            ),
    );
  }

  void callUID() async {
    Response response =
        await get("${Config.urlHTTP}/uid", headers: <String, String>{
      'Accept': 'application/json; charset=UTF-8',
      'Content-Type': 'application/json; charset=UTF-8',
    });

    final jsonResponse = json.decode(response.body.toString());

    setState(() {
      uid = UID.fromJson(jsonResponse);
      if (uid.status == 200) {
        status = true;
      }
    });
  }

  void deleteUID(String text) async {
    print("deee ");
    await pr.show();
    final jsonsp = jsonEncode({"text": text});

    print(json.decode.toString());
    Response response = await post("${Config.urlHTTP}/uuid/delete",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonsp);

    final jsonResponse = json.decode(response.body.toString());

    if (jsonResponse['status'] == 200) {
      pr.hide();
      _showMyDialog("ลบสำเร็จ");
      Navigator.of(context).pop();
      callUID();
    } else {
      pr.hide();
    }
  }

  Future<void> _showMyDialogDelete(String text) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text('UID'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("คุณต้องการลบ ${text} ใช่หรือไม่"),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('ยกเลิก'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('ตกลง'),
              onPressed: () {
                Navigator.of(context).pop();
                deleteUID(text);
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> _showMyDialog(String text) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text('Setting'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("${text}"),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('ตกลง'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
