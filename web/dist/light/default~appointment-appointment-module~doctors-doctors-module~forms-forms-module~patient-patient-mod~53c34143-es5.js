(function () {
  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

  function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

  function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

  function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

  function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

  function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

  function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~appointment-appointment-module~doctors-doctors-module~forms-forms-module~patient-patient-mod~53c34143"], {
    /***/
    "./node_modules/ngx-material-file-input/__ivy_ngcc__/fesm2015/ngx-material-file-input.js":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/ngx-material-file-input/__ivy_ngcc__/fesm2015/ngx-material-file-input.js ***!
      \***********************************************************************************************/

    /*! exports provided: ByteFormatPipe, FileInput, FileInputComponent, FileInputConfig, FileValidator, MaterialFileInputModule, NGX_MAT_FILE_INPUT_CONFIG, ɵa, ɵb */

    /***/
    function node_modulesNgxMaterialFileInput__ivy_ngcc__Fesm2015NgxMaterialFileInputJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ByteFormatPipe", function () {
        return ByteFormatPipe;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FileInput", function () {
        return FileInput;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FileInputComponent", function () {
        return FileInputComponent;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FileInputConfig", function () {
        return FileInputConfig;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FileValidator", function () {
        return FileValidator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MaterialFileInputModule", function () {
        return MaterialFileInputModule;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NGX_MAT_FILE_INPUT_CONFIG", function () {
        return NGX_MAT_FILE_INPUT_CONFIG;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ɵa", function () {
        return FileInputBase;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ɵb", function () {
        return FileInputMixinBase;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/cdk/a11y */
      "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/a11y.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_material_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/material/core */
      "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/material/form-field */
      "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/form-field.js");
      /* harmony import */


      var _angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/cdk/coercion */
      "./node_modules/@angular/cdk/fesm2015/coercion.js");
      /**
       * Optional token to provide custom configuration to the module
       */


      var NGX_MAT_FILE_INPUT_CONFIG = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["InjectionToken"]('ngx-mat-file-input.config');
      /**
       * Provide additional configuration to dynamically customize the module injection
       */

      var FileInputConfig = function FileInputConfig() {
        _classCallCheck(this, FileInputConfig);
      };
      /**
       * The files to be uploaded
       */


      var FileInput = /*#__PURE__*/function () {
        function FileInput(_files) {
          var delimiter = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : ', ';

          _classCallCheck(this, FileInput);

          this._files = _files;
          this.delimiter = delimiter;
          this._fileNames = (this._files || []).map(function (f) {
            return f.name;
          }).join(delimiter);
        }

        _createClass(FileInput, [{
          key: "files",
          get: function get() {
            return this._files || [];
          }
        }, {
          key: "fileNames",
          get: function get() {
            return this._fileNames;
          }
        }]);

        return FileInput;
      }(); // Boilerplate for applying mixins to FileInput

      /** @docs-private */


      var FileInputBase = function FileInputBase(_defaultErrorStateMatcher, _parentForm, _parentFormGroup, ngControl) {
        _classCallCheck(this, FileInputBase);

        this._defaultErrorStateMatcher = _defaultErrorStateMatcher;
        this._parentForm = _parentForm;
        this._parentFormGroup = _parentFormGroup;
        this.ngControl = ngControl;
      };
      /**
       * Allows to use a custom ErrorStateMatcher with the file-input component
       */


      var FileInputMixinBase = Object(_angular_material_core__WEBPACK_IMPORTED_MODULE_4__["mixinErrorState"])(FileInputBase);
      var FileInputComponent_1;

      var FileInputComponent = FileInputComponent_1 = /*#__PURE__*/function (_FileInputMixinBase) {
        _inherits(FileInputComponent, _FileInputMixinBase);

        var _super = _createSuper(FileInputComponent);

        /**
         * @see https://angular.io/api/forms/ControlValueAccessor
         */
        function FileInputComponent(fm, _elementRef, _renderer, _defaultErrorStateMatcher, ngControl, _parentForm, _parentFormGroup) {
          var _this;

          _classCallCheck(this, FileInputComponent);

          _this = _super.call(this, _defaultErrorStateMatcher, _parentForm, _parentFormGroup, ngControl);
          _this.fm = fm;
          _this._elementRef = _elementRef;
          _this._renderer = _renderer;
          _this._defaultErrorStateMatcher = _defaultErrorStateMatcher;
          _this.ngControl = ngControl;
          _this._parentForm = _parentForm;
          _this._parentFormGroup = _parentFormGroup;
          _this.focused = false;
          _this.controlType = 'file-input';
          _this.autofilled = false;
          _this._required = false;
          _this.accept = null;
          _this.id = "ngx-mat-file-input-".concat(FileInputComponent_1.nextId++);
          _this.describedBy = '';

          _this._onChange = function (_) {};

          _this._onTouched = function () {};

          if (_this.ngControl != null) {
            _this.ngControl.valueAccessor = _assertThisInitialized(_this);
          }

          fm.monitor(_elementRef.nativeElement, true).subscribe(function (origin) {
            _this.focused = !!origin;

            _this.stateChanges.next();
          });
          return _this;
        }

        _createClass(FileInputComponent, [{
          key: "setDescribedByIds",
          value: function setDescribedByIds(ids) {
            this.describedBy = ids.join(' ');
          }
        }, {
          key: "onContainerClick",
          value: function onContainerClick(event) {
            if (event.target.tagName.toLowerCase() !== 'input' && !this.disabled) {
              this._elementRef.nativeElement.querySelector('input').focus();

              this.focused = true;
              this.open();
            }
          }
        }, {
          key: "writeValue",
          value: function writeValue(obj) {
            this._renderer.setProperty(this._elementRef.nativeElement, 'value', obj instanceof FileInput ? obj.files : null);
          }
        }, {
          key: "registerOnChange",
          value: function registerOnChange(fn) {
            this._onChange = fn;
          }
        }, {
          key: "registerOnTouched",
          value: function registerOnTouched(fn) {
            this._onTouched = fn;
          }
          /**
           * Remove all files from the file input component
           * @param [event] optional event that may have triggered the clear action
           */

        }, {
          key: "clear",
          value: function clear(event) {
            if (event) {
              event.preventDefault();
              event.stopPropagation();
            }

            this.value = new FileInput([]);
            this._elementRef.nativeElement.querySelector('input').value = null;

            this._onChange(this.value);
          }
        }, {
          key: "change",
          value: function change(event) {
            var fileList = event.target.files;
            var fileArray = [];

            if (fileList) {
              for (var i = 0; i < fileList.length; i++) {
                fileArray.push(fileList[i]);
              }
            }

            this.value = new FileInput(fileArray);

            this._onChange(this.value);
          }
        }, {
          key: "blur",
          value: function blur() {
            this.focused = false;

            this._onTouched();
          }
        }, {
          key: "setDisabledState",
          value: function setDisabledState(isDisabled) {
            this._renderer.setProperty(this._elementRef.nativeElement, 'disabled', isDisabled);
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            this.multiple = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_6__["coerceBooleanProperty"])(this.multiple);
          }
        }, {
          key: "open",
          value: function open() {
            if (!this.disabled) {
              this._elementRef.nativeElement.querySelector('input').click();
            }
          }
        }, {
          key: "ngOnDestroy",
          value: function ngOnDestroy() {
            this.stateChanges.complete();
            this.fm.stopMonitoring(this._elementRef.nativeElement);
          }
        }, {
          key: "ngDoCheck",
          value: function ngDoCheck() {
            if (this.ngControl) {
              // We need to re-evaluate this on every change detection cycle, because there are some
              // error triggers that we can't subscribe to (e.g. parent form submissions). This means
              // that whatever logic is in here has to be super lean or we risk destroying the performance.
              this.updateErrorState();
            }
          }
        }, {
          key: "value",
          get: function get() {
            return this.empty ? null : new FileInput(this._elementRef.nativeElement.value || []);
          },
          set: function set(fileInput) {
            if (fileInput) {
              this.writeValue(fileInput);
              this.stateChanges.next();
            }
          }
        }, {
          key: "placeholder",
          get: function get() {
            return this._placeholder;
          },
          set: function set(plh) {
            this._placeholder = plh;
            this.stateChanges.next();
          }
          /**
           * Whether the current input has files
           */

        }, {
          key: "empty",
          get: function get() {
            return !this._elementRef.nativeElement.value || this._elementRef.nativeElement.value.length === 0;
          }
        }, {
          key: "shouldLabelFloat",
          get: function get() {
            return this.focused || !this.empty || this.valuePlaceholder !== undefined;
          }
        }, {
          key: "required",
          get: function get() {
            return this._required;
          },
          set: function set(req) {
            this._required = Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_6__["coerceBooleanProperty"])(req);
            this.stateChanges.next();
          }
        }, {
          key: "isDisabled",
          get: function get() {
            return this.disabled;
          }
        }, {
          key: "disabled",
          get: function get() {
            return this._elementRef.nativeElement.disabled;
          },
          set: function set(dis) {
            this.setDisabledState(Object(_angular_cdk_coercion__WEBPACK_IMPORTED_MODULE_6__["coerceBooleanProperty"])(dis));
            this.stateChanges.next();
          }
        }, {
          key: "fileNames",
          get: function get() {
            return this.value ? this.value.fileNames : this.valuePlaceholder;
          }
        }]);

        return FileInputComponent;
      }(FileInputMixinBase);

      FileInputComponent.ɵfac = function FileInputComponent_Factory(t) {
        return new (t || FileInputComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_2__["FocusMonitor"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_core__WEBPACK_IMPORTED_MODULE_4__["ErrorStateMatcher"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"], 10), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"], 8), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"], 8));
      };

      FileInputComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: FileInputComponent,
        selectors: [["ngx-mat-file-input"]],
        hostVars: 6,
        hostBindings: function FileInputComponent_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function FileInputComponent_change_HostBindingHandler($event) {
              return ctx.change($event);
            })("focusout", function FileInputComponent_focusout_HostBindingHandler() {
              return ctx.blur();
            });
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵhostProperty"]("id", ctx.id);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("aria-describedby", ctx.describedBy);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("mat-form-field-should-float", ctx.shouldLabelFloat)("file-input-disabled", ctx.isDisabled);
          }
        },
        inputs: {
          autofilled: "autofilled",
          accept: "accept",
          value: "value",
          placeholder: "placeholder",
          required: "required",
          disabled: "disabled",
          multiple: "multiple",
          valuePlaceholder: "valuePlaceholder",
          errorStateMatcher: "errorStateMatcher"
        },
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([{
          provide: _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__["MatFormFieldControl"],
          useExisting: FileInputComponent_1
        }]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵInheritDefinitionFeature"]],
        decls: 4,
        vars: 4,
        consts: [["type", "file"], ["input", ""], [1, "filename", 3, "title"]],
        template: function FileInputComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "input", 0, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("multiple", ctx.multiple ? "" : null)("accept", ctx.accept);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("title", ctx.fileNames);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.fileNames);
          }
        },
        styles: ["[_nghost-%COMP%]{display:inline-block;width:100%}[_nghost-%COMP%]:not(.file-input-disabled){cursor:pointer}input[_ngcontent-%COMP%]{width:0;height:0;opacity:0;overflow:hidden;position:absolute;z-index:-1}.filename[_ngcontent-%COMP%]{display:inline-block;text-overflow:ellipsis;overflow:hidden;width:100%}"]
      });
      FileInputComponent.nextId = 0;

      FileInputComponent.ctorParameters = function () {
        return [{
          type: _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_2__["FocusMonitor"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]
        }, {
          type: _angular_material_core__WEBPACK_IMPORTED_MODULE_4__["ErrorStateMatcher"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"],
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Self"]
          }]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"],
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
          }]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"],
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
          }]
        }];
      };

      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()], FileInputComponent.prototype, "autofilled", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()], FileInputComponent.prototype, "valuePlaceholder", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()], FileInputComponent.prototype, "multiple", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()], FileInputComponent.prototype, "accept", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()], FileInputComponent.prototype, "errorStateMatcher", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])()], FileInputComponent.prototype, "id", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('attr.aria-describedby')], FileInputComponent.prototype, "describedBy", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()], FileInputComponent.prototype, "value", null);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()], FileInputComponent.prototype, "placeholder", null);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.mat-form-field-should-float')], FileInputComponent.prototype, "shouldLabelFloat", null);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()], FileInputComponent.prototype, "required", null);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.file-input-disabled')], FileInputComponent.prototype, "isDisabled", null);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])()], FileInputComponent.prototype, "disabled", null);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('change', ['$event'])], FileInputComponent.prototype, "change", null);
      Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('focusout')], FileInputComponent.prototype, "blur", null);
      FileInputComponent = FileInputComponent_1 = Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__param"])(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()), Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__param"])(4, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Self"])()), Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__param"])(5, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()), Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__param"])(6, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])())], FileInputComponent);

      var ByteFormatPipe = /*#__PURE__*/function () {
        function ByteFormatPipe(config) {
          _classCallCheck(this, ByteFormatPipe);

          this.config = config;
          this.unit = config ? config.sizeUnit : 'Byte';
        }

        _createClass(ByteFormatPipe, [{
          key: "transform",
          value: function transform(value, args) {
            if (parseInt(value, 10) >= 0) {
              value = this.formatBytes(+value, +args);
            }

            return value;
          }
        }, {
          key: "formatBytes",
          value: function formatBytes(bytes, decimals) {
            if (bytes === 0) {
              return '0 ' + this.unit;
            }

            var B = this.unit.charAt(0);
            var k = 1024;
            var dm = decimals || 2;
            var sizes = [this.unit, 'K' + B, 'M' + B, 'G' + B, 'T' + B, 'P' + B, 'E' + B, 'Z' + B, 'Y' + B];
            var i = Math.floor(Math.log(bytes) / Math.log(k));
            return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
          }
        }]);

        return ByteFormatPipe;
      }();

      ByteFormatPipe.ɵfac = function ByteFormatPipe_Factory(t) {
        return new (t || ByteFormatPipe)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](NGX_MAT_FILE_INPUT_CONFIG, 8));
      };

      ByteFormatPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({
        name: "byteFormat",
        type: ByteFormatPipe,
        pure: true
      });

      ByteFormatPipe.ctorParameters = function () {
        return [{
          type: FileInputConfig,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
            args: [NGX_MAT_FILE_INPUT_CONFIG]
          }]
        }];
      };

      ByteFormatPipe = Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__param"])(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()), Object(tslib__WEBPACK_IMPORTED_MODULE_1__["__param"])(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(NGX_MAT_FILE_INPUT_CONFIG))], ByteFormatPipe);

      var MaterialFileInputModule = function MaterialFileInputModule() {
        _classCallCheck(this, MaterialFileInputModule);
      };

      MaterialFileInputModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: MaterialFileInputModule
      });
      MaterialFileInputModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function MaterialFileInputModule_Factory(t) {
          return new (t || MaterialFileInputModule)();
        },
        providers: [_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_2__["FocusMonitor"]]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FileInputComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            // tslint:disable-next-line:component-selector
            selector: 'ngx-mat-file-input',
            template: "<input #input type=\"file\" [attr.multiple]=\"multiple? '' : null\" [attr.accept]=\"accept\">\n<span class=\"filename\" [title]=\"fileNames\">{{ fileNames }}</span>\n",
            providers: [{
              provide: _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__["MatFormFieldControl"],
              useExisting: FileInputComponent_1
            }],
            styles: [":host{display:inline-block;width:100%}:host:not(.file-input-disabled){cursor:pointer}input{width:0;height:0;opacity:0;overflow:hidden;position:absolute;z-index:-1}.filename{display:inline-block;text-overflow:ellipsis;overflow:hidden;width:100%}"]
          }]
        }], function () {
          return [{
            type: _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_2__["FocusMonitor"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]
          }, {
            type: _angular_material_core__WEBPACK_IMPORTED_MODULE_4__["ErrorStateMatcher"]
          }, {
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgControl"],
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
            }, {
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Self"]
            }]
          }, {
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["NgForm"],
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
            }]
          }, {
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroupDirective"],
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
            }]
          }];
        }, {
          autofilled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          accept: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          id: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"]
          }],
          describedBy: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"],
            args: ['attr.aria-describedby']
          }],
          value: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          placeholder: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          shouldLabelFloat: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"],
            args: ['class.mat-form-field-should-float']
          }],
          required: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          isDisabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"],
            args: ['class.file-input-disabled']
          }],
          disabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          change: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['change', ['$event']]
          }],
          blur: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['focusout']
          }],
          multiple: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          valuePlaceholder: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          errorStateMatcher: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ByteFormatPipe, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
          args: [{
            name: 'byteFormat'
          }]
        }], function () {
          return [{
            type: FileInputConfig,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"]
            }, {
              type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"],
              args: [NGX_MAT_FILE_INPUT_CONFIG]
            }]
          }];
        }, null);
      })();

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MaterialFileInputModule, {
          declarations: [FileInputComponent, ByteFormatPipe],
          exports: [FileInputComponent, ByteFormatPipe]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MaterialFileInputModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [FileInputComponent, ByteFormatPipe],
            providers: [_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_2__["FocusMonitor"]],
            exports: [FileInputComponent, ByteFormatPipe]
          }]
        }], null, null);
      })();

      var FileValidator;

      (function (FileValidator) {
        /**
         * Function to control content of files
         *
         * @param bytes max number of bytes allowed
         *
         * @returns
         */
        function maxContentSize(bytes) {
          return function (control) {
            var size = control && control.value ? control.value.files.map(function (f) {
              return f.size;
            }).reduce(function (acc, i) {
              return acc + i;
            }, 0) : 0;
            var condition = bytes >= size;
            return condition ? null : {
              maxContentSize: {
                actualSize: size,
                maxSize: bytes
              }
            };
          };
        }

        FileValidator.maxContentSize = maxContentSize;
      })(FileValidator || (FileValidator = {}));
      /**
       * Generated bundle index. Do not edit.
       */
      //# sourceMappingURL=ngx-material-file-input.js.map

      /***/

    }
  }]);
})();
//# sourceMappingURL=default~appointment-appointment-module~doctors-doctors-module~forms-forms-module~patient-patient-mod~53c34143-es5.js.map