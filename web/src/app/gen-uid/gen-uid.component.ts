import { HttpClient } from '@angular/common/http';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-gen-uid',
  templateUrl: './gen-uid.component.html',
  styleUrls: ['./gen-uid.component.sass']
})
export class GenUidComponent implements OnInit {

  @ViewChild('roleTemplate', { static: true }) roleTemplate: TemplateRef<any>;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  @ViewChild('closeAddModal', { static: false }) closeAddModal;
  @ViewChild('closeEditModal', { static: false }) closeEditModal;
  rows = [];
  newUserImg = 'assets/images/user/user1.jpg';
  data = [];
  filteredData = [];


  uid = new FormControl("")
  note = new FormControl("")

  selectedOption: string;
  columns = [

    { prop: "uid" },
    { prop: "note" },

  ];


  

  @ViewChild(DatatableComponent, { static: false }) table2: DatatableComponent;
  constructor(private http: HttpClient) {

  }


  dataList = [
  ]

  ngOnInit() {

    this.callAPI();

  }

  createUID(){

    var data =
    {
        "uuid" : this.uid.value,
        "note" : this.note.value
    }

    this.http.post("http://35.240.153.210:8989/uuid", data)
      .subscribe(data => {

        this.callAPI();
      })
  }


  getUID() {

    console.log("UID");

    this.http.get("http://35.240.153.210:8989/uuid", {})

      .subscribe(

        data => {

          if (data["status"] == 200) {
            this.uid.setValue(data["uid"]);
          }
        })
  }
  callAPI() {

    this.dataList = [];
    this.filteredData = [];
    this.http.get("http://35.240.153.210:8989/uid", {})

      .subscribe(

        data => {

          var json = JSON.parse(JSON.stringify(data))

          if (json["status"] == 200) {

            for (var i = 0; i < Object.keys(json['message']).length; i++) {
              var raw = {};

              raw["uid"] = json["message"][i]["text"]
              raw["note"] = json["message"][i]["note"]

              this.dataList.push(raw);
            }
            this.data = this.dataList;
            this.filteredData = this.dataList;

          }



        })
  }

  arrayRemove(array, id) {
    return array.filter(function (element) {
      return element.id != id;
    });
  }

  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match
        if (
          item[keys[i]]
            .toString()
            .toLowerCase()
            .indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });

    console.log(JSON.stringify(this.data))
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
  getId(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

}

