import { HttpClient } from '@angular/common/http';
import { Component, ViewChild, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IMqttMessage, MqttService } from 'ngx-mqtt';
import { Subscription } from 'rxjs';

import * as CanvasJS from '../../assets/canvasjs.min';
@Component({
  selector: 'app-realtime',
  templateUrl: './realtime.component.html',
  styleUrls: ['./realtime.component.sass']
})
export class RealtimeComponent implements OnInit {
  private subscription: Subscription;
  public message: string;

  uid: string;
  aqi: number;

  pm25 = 0.0
  pm10 = 0.0
  co = 0.0
  o3 = 0.0
  no2 = 0.0
  so2 = 0.0
  name
  dt
  image = "/assets/images/aqi01.png"

  data
  chart

  listPM25 = [];
  listPM10 = [];
  constructor(private http: HttpClient, public router: Router, public route: ActivatedRoute, private _mqttService: MqttService) {



    this.route.params.subscribe(params => {
      console.log("params" + JSON.stringify(params))
      this.uid = params['uid']
      this.aqi = params['aqi']


      this.getLast();

      this.name = params["name"]
      this.subscription = this._mqttService.observe(this.uid).subscribe((message: IMqttMessage) => {
        this.message = message.payload.toString();

        console.log(message.payload.toString());


        var json = JSON.parse(this.message);


        this.pm25 = json["pm25"]
        this.co = json["co"]
        this.pm10 = json["pm10"]
        this.so2 = json["so2"]
        this.o3 = json["o3"]
        this.no2 = json["no2"]


        if (json["aqi"] >= 0 && json["aqi"] <= 25) {
          this.image = "/assets/images/aqi01.png"
        } else if (json["aqi"] >= 26 && json["aqi"] <= 50) {
          this.image = "/assets/images/aqi02.png"
        } else if (json["aqi"] >= 51 && json["aqi"] <= 100) {
          this.image = "/assets/images/aqi03.png"
        } else if (json["aqi"] >= 101 && json["aqi"] <= 200) {
          this.image = "/assets/images/aqi04.png"
        } else {
          this.image = "/assets/images/aqi05.png"
        }



        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var MM = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = today.getFullYear();

        var hh = String(today.getHours()).padStart(2, '0');
        var mm = String(today.getMinutes()).padStart(2, '0');
        var ss = String(today.getSeconds()).padStart(2, '0');
        this.dt = dd + "/" + MM + "/" + yyyy + " " + hh + ":" + mm + ":" + ss



        var raw25 = {};

        raw25["y"] = json["pm25"];
        raw25["label"] = hh + ":" + mm + ":" + ss;

        this.listPM25.push(raw25);

        var raw10 = {};

        raw10["y"] = json["pm10"];
        raw10["label"] = hh + ":" + mm + ":" + ss;

        this.listPM10.push(raw10);


        this.chart.data[0] = this.listPM25;
        this.chart.data[1] = this.listPM10;

        this.chart.render();
      });

    })
  }



  getLast() {

    this.http.get("http://35.240.153.210:8989/log/last/" + this.uid + "/" + 10)
      .subscribe(data => {

        console.log(JSON.stringify(data));

        if (data["status"] == 200) {

          this.pm25 = data["message"][0]["pm25"]
          this.co = data["message"][0]["co"]
          this.pm10 = data["message"][0]["pm10"]
          this.so2 = data["message"][0]["so2"]
          this.o3 = data["message"][0]["o3"]
          this.no2 = data["message"][0]["no2"]




          for (var i = data["message"].length - 1; i >= 0; i--) {



            var raw25 = {};

            raw25["y"] = data["message"][i]["pm25"];
            raw25["label"] = data["message"][i]["dt"];

            this.listPM25.push(raw25);

            var raw10 = {};

            raw10["y"] = data["message"][i]["pm10"];
            raw10["label"] = data["message"][i]["dt"];

            this.listPM10.push(raw10);


          }

          this.chart.data[0] = this.listPM25;
          this.chart.data[1] = this.listPM10;

          this.chart.render();








        }
      })
  }

  select() {

    this.router.navigateByUrl('/graph/' + this.uid);

  }


  ngOnInit(): void {

    this.chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,
      title: {
        text: "กราฟ RealTime"
      },

      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: true,
        itemclick: (e) => {

          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          this.chart.render();

        }
      },

      data: [

        {
          type: "spline",
          name: "PM2.5",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# ug/m3",
          dataPoints: this.listPM25
        },


        {
          type: "spline",
          name: "PM10",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# ug/m3",
          dataPoints: this.listPM10
        }


      ]
    });

    this.chart.render();

  }

}
