from coapthon.client.helperclient import HelperClient
from coapthon import defines

host = "35.240.153.210"
port = 3122
path ="log/"

client = HelperClient(server=(host, port))

# response = client.get(path)

payload = '{"uid":"b0ff","aqi":116.23,"pm25":57.00,"pm10":60.00,"so2":0.23,"o3":33.75,"co":0.00,"no2":0.00}'

response = client.post(path,payload)

print (response.pretty_print())


client.stop()