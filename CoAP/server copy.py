from coapthon.server.coap import CoAP
from coapthon import defines
from coapthon.resources.resource import Resource
import json
from datetime import datetime




class AdvancedResource(Resource):
    def __init__(self, name="Advanced"):
        super(AdvancedResource, self).__init__(name)
        self.payload = "Advanced resource"

    def render_GET_advanced(self, request, response):
        response.payload = self.payload
        response.max_age = 20
        response.code = defines.Codes.CONTENT.number
        return self, response

    def render_POST_advanced(self, request, response):

        from coapthon.messages.response import Response
        assert(isinstance(response, Response))


        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

        response.payload = (defines.Content_types["application/json"],json.dumps({"status":"ok" , "dt" : dt_string}))

        print(request.payload)
        response.code = defines.Codes.CREATED.number
        return self, response


class CoAPServer(CoAP):
    def __init__(self, host, port , multicast=False):
        CoAP.__init__(self, (host, port) , multicast)
        self.add_resource('basic/', AdvancedResource())

def main():
    multicast = False
    server = CoAPServer("0.0.0.0", 5555 , multicast)
    try:
        server.listen(10)
    except KeyboardInterrupt:
        print ("Server Shutdown")
        server.close()
        print ("Exiting...")

if __name__ == '__main__':
    main()